<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Daftarmall Controller
 *
 * @property \App\Model\Table\DaftarmallTable $Daftarmall
 *
 * @method \App\Model\Entity\Daftarmall[] paginate($object = null, array $settings = [])
 */
class DaftarmallController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $daftarmall = $this->paginate($this->Daftarmall);

        $this->set(compact('daftarmall'));
        $this->set('_serialize', ['daftarmall']);
    }

    /**
     * View method
     *
     * @param string|null $id Daftarmall id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $daftarmall = $this->Daftarmall->get($id, [
            'contain' => []
        ]);

        $this->set('daftarmall', $daftarmall);
        $this->set('_serialize', ['daftarmall']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $daftarmall = $this->Daftarmall->newEntity();
        if ($this->request->is('post')) {
            $daftarmall = $this->Daftarmall->patchEntity($daftarmall, $this->request->getData());
            if ($this->Daftarmall->save($daftarmall)) {
                $this->Flash->success(__('The daftarmall has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daftarmall could not be saved. Please, try again.'));
        }
        $this->set(compact('daftarmall'));
        $this->set('_serialize', ['daftarmall']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Daftarmall id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $daftarmall = $this->Daftarmall->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $daftarmall = $this->Daftarmall->patchEntity($daftarmall, $this->request->getData());
            if ($this->Daftarmall->save($daftarmall)) {
                $this->Flash->success(__('The daftarmall has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The daftarmall could not be saved. Please, try again.'));
        }
        $this->set(compact('daftarmall'));
        $this->set('_serialize', ['daftarmall']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Daftarmall id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $daftarmall = $this->Daftarmall->get($id);
        if ($this->Daftarmall->delete($daftarmall)) {
            $this->Flash->success(__('The daftarmall has been deleted.'));
        } else {
            $this->Flash->error(__('The daftarmall could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
