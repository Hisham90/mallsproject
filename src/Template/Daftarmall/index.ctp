<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Daftarmall[]|\Cake\Collection\CollectionInterface $daftarmall
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Daftarmall'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="daftarmall index large-9 medium-8 columns content">
    <h3><?= __('Daftar Mall') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
              
                <th scope="col"><?= $this->Paginator->sort('Malls Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('About') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('State') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Contact') ?></th>
               
               
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($daftarmall as $daftarmall): ?>
            <tr>
               
                <td><?= h($daftarmall->mallsname) ?></td>
                <td><?= h($daftarmall->about) ?></td>
                <td><?= h($daftarmall->alamat) ?></td>
                <td><?= h($daftarmall->negeri) ?></td>
                <td><?= h($daftarmall->contact) ?></td>
                
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $daftarmall->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $daftarmall->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $daftarmall->id], ['confirm' => __('Are you sure you want to delete  {0}?', $daftarmall->mallsname)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
