<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Daftarmall $daftarmall
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Daftarmall'), ['action' => 'edit', $daftarmall->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Daftarmall'), ['action' => 'delete', $daftarmall->id], ['confirm' => __('Are you sure you want to delete # {0}?', $daftarmall->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Daftarmall'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Daftarmall'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="daftarmall view large-9 medium-8 columns content">
    <h3><?= h($daftarmall->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Mallsname') ?></th>
            <td><?= h($daftarmall->mallsname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('About') ?></th>
            <td><?= h($daftarmall->about) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alamat') ?></th>
            <td><?= h($daftarmall->alamat) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Negeri') ?></th>
            <td><?= h($daftarmall->negeri) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact') ?></th>
            <td><?= h($daftarmall->contact) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($daftarmall->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($daftarmall->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($daftarmall->modified) ?></td>
        </tr>
    </table>
</div>
