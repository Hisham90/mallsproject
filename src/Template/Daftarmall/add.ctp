<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Daftar Mall'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="daftarmall form large-9 medium-8 columns content">
    <?= $this->Form->create($daftarmall) ?>
    <fieldset>
        <legend><?= __('Daftar Malls') ?></legend>
        <?php


            echo $this->Form->control('mallsname');
            echo $this->Form->control('about');
            echo $this->Form->control('alamat');
            echo $this->Form->control('negeri');
            echo $this->Form->control('contact');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
