<?php
/**
 * @var \App\View\AppView $this
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $daftarmall->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $daftarmall->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Daftarmall'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="daftarmall form large-9 medium-8 columns content">
    <?= $this->Form->create($daftarmall) ?>
    <fieldset>
        <legend><?= __('Edit Daftarmall') ?></legend>
        <?php
            echo $this->Form->control('mallsname');
            echo $this->Form->control('about');
            echo $this->Form->control('alamat');
            echo $this->Form->control('negeri');
            echo $this->Form->control('contact');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
