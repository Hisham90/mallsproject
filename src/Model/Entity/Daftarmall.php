<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Daftarmall Entity
 *
 * @property int $id
 * @property string $mallsname
 * @property string $about
 * @property string $alamat
 * @property string $negeri
 * @property string $contact
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Daftarmall extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'mallsname' => true,
        'about' => true,
        'alamat' => true,
        'negeri' => true,
        'contact' => true,
        'created' => true,
        'modified' => true
    ];
}
