<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Daftarmall Model
 *
 * @method \App\Model\Entity\Daftarmall get($primaryKey, $options = [])
 * @method \App\Model\Entity\Daftarmall newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Daftarmall[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Daftarmall|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Daftarmall patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Daftarmall[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Daftarmall findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DaftarmallTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('daftarmall');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('mallsname')
            ->requirePresence('mallsname', 'create')
            ->notEmpty('mallsname');

        $validator
            ->scalar('about')
            ->requirePresence('about', 'create')
            ->notEmpty('about');

        $validator
            ->scalar('alamat')
            ->requirePresence('alamat', 'create')
            ->notEmpty('alamat');

        $validator
            ->scalar('negeri')
            ->requirePresence('negeri', 'create')
            ->notEmpty('negeri');

        $validator
            ->scalar('contact')
            ->requirePresence('contact', 'create')
            ->notEmpty('contact');

        return $validator;
    }
}
