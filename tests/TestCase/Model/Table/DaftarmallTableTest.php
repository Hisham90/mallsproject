<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DaftarmallTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DaftarmallTable Test Case
 */
class DaftarmallTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DaftarmallTable
     */
    public $Daftarmall;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.daftarmall'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Daftarmall') ? [] : ['className' => DaftarmallTable::class];
        $this->Daftarmall = TableRegistry::get('Daftarmall', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Daftarmall);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
